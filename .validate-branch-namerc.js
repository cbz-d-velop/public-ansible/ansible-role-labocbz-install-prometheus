module.exports = {
    pattern: '^(develop|main|fork-\\d{1,9}_[A-Za-z0-9_]+)$',
    errorMsg:
        'Your branch doesn\'t follow Git branch namening guidelines.',
    }
